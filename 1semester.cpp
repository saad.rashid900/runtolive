#include<iostream>
#include<fstream>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#include<windows.h>

using namespace std;




//variables
const int X=18;
const int Y=32;
char map1[X][Y];
int moveX=14;
int moveY=18;
bool gameruning=true; 
char map2[X][Y];
int ghostx1=2;
int ghosty1=5;
int ghostx2=0;
int ghosty2=0;
int rightLimit=2;
int leftLimit=30;
int higgestscore1=0;
int higgestscore2=0;
string ghostDirection1="right";
string ghostDirection2="left";





//functions
void loadData();
void saveData();
void showmap1();
void showmap2();
void gotoxy( short x, short y );
bool checkindex(int life1);
int menue();
void intro();
int higgestscores1(int score1);
int higgestscores2(int score2);
bool lapplay();
int option1(bool lapplays);
void dollarmovement();
void moveleft();
void moveright();
void moveup();
void movedown();
void ghoostmovement1();
void ghoostmovement2();
int luckyindex1();
int luckselection1(int luck1,int life1,int score1);
int luckyindex2();
int luckselection2(int luck2,int life2,int score2);
void usermovement();
int scorecounter();
void printscore(int score);
void ghoost1();
void ghoost2();
void dollarplacement();



int main()
{
    system("cls");
    int score,option=100;
    intro();
    while(option)
    {
        system("cls");
        intro();
        option=menue();
        if(option==1)
        {
            bool lapplays=false;
            int op1=100;
            op1=option1(lapplays);
            cout<<op1;
            if(op1==1)
            {
                system("cls");
                gameruning=true;
                showmap1();
                ghoost1();
                dollarplacement();
                int life1=2,luck=100,score1;
                while(gameruning)
                {
                    //usermovement();
                    //dollarmovement();
                    gameruning=checkindex(life1);
                    luck=luckyindex1();
                    luckselection1(luck,life1,score1);
                    score1=scorecounter();
                    printscore(score1);
                    ghoostmovement1();
                    gameruning=checkindex(life1);
                }
                system("cls");
                cout<<"YOUR SCORE= "<<score1;    
                higgestscore1=higgestscores1(score1);
            }
            if(op1==2)
            {
                system("cls");
                gameruning=true;
                showmap2();
                ghoost1();
                ghoost2();
                int life2=2,luck2=100,score2;
                while(gameruning)
                {
                    usermovement();
                    dollarmovement();
                    luck2=luckyindex2();
                    luckselection2(luck2,life2,score2);
                    score2=scorecounter();
                    printscore(score2);
                    ghoostmovement1();
                    ghoostmovement2();
                    gameruning=checkindex(life2);
                }
                system("cls");
                cout<<"YOUR SCORE= "<<score2;    
                higgestscore2=higgestscores2(score2);
            }
        }
        else if(option==2)
        {
            system("cls");
            cout<< "LAP1" <<endl;
            cout<< "HIGGEST SCORE=" <<higgestscore1<<endl;
            if(lapplay)
            {
                cout<<"LAP2"<<endl;
                cout<<"HIGGEST SCORE="<<higgestscore2;
            }
            getch();
        }
        else if(option==3)
        {
            system("cls");
            cout<<" 1-LAP "<<endl;
            showmap1();
            ghoost1();
            getch();
            system("cls");
            cout<<" 2-LAP "<<endl;
            showmap2();
            ghoost2();
            getch();
        }
        else if(option==4)
        {
            return 0;
        }
    }
}




void loadData()
{
    fstream newfile;
    string record;
    int x=0;
    newfile.open("f://game/gamemap.txt",ios::in);
    while(getline(newfile,record))
    {
        for(int x=0;x<=18;x++)
        {
            for(int y=0;y<=32;y++)
            {
                map1[x][y]=record[y];
            }
        }
    }
    newfile.close();
}



void storeData()
{
    fstream file;
    file.open("gamemap.txt",ios::out);
    file.close();
}




void showmap1()
{
    char map1[18][32] = 
    { 
    "|-----------------------------|",
    "|%%%%%  ######   %%%%%%%......|",
    "|                             |",
    "|----.........%%%%++++  ------|",
    "|                             |",
    "|                             |",
    "|                             |",
    "|############.+.+.+ ----------|",
    "|                             |",
    "|------...^^^-----------------|",
    "|.............................|",
    "|------------ ^^  ##### .. ---|",
    "|                             |",
    "|%%%%%%%   ++++ %%%%%%%%%%%%%%|",
    "|...........                  |",
    "|         ++++++++++++++++++++|",
    "|                             |",
    "|-----------------------------|"
    };
    for(int x=0;x<18;x++)
    {
        for(int y=0;y<32;y++)
        {
            cout<<map1[x][y];
        }
        cout<<endl;
    }
}




void showmap2()
{
    for(int x=0;x<18;x++)
    {
        for(int y=0;y<32;y++)
        {
            cout<<map2[x][y];
        }
        cout<<endl;
    }
}




void moveleft()
{
    gotoxy(moveX,moveY);
    cout<<" ";
    moveX=moveX-1;
    gotoxy(moveX,moveY);
    cout<<"$";      
}




void moveright()
{
    gotoxy(moveX,moveY);
    cout<<" ";
    moveX=moveX+1;
    gotoxy(moveX,moveY);
    cout<<"$";      
}






void moveup()
{
    gotoxy(moveX,moveY);
    cout<<" ";
    moveY=moveY-1;
    gotoxy(moveX,moveY);
    cout<<"$";            
}






void movedown()
{
    gotoxy(moveX,moveY);
    cout<<" ";
    moveY=moveY+1;
    gotoxy(moveX,moveY);
    cout<<"$";    
}





bool checkindex(int life1)
{
    bool gameruning=true;
    if( ( map1[moveX][moveY]=='#' || map1[moveX][moveY]=='G' || (moveX=ghostx1) && (moveY=ghosty1)|| map1[moveX][moveY]=='%' || map1[moveX][moveY]=='-' ) || ( map2[moveX][moveY]=='#' || map2[moveX][moveY]=='G' || map2[moveX][moveY]=='|' || map2[moveX][moveY]=='%' || map1[moveX][moveY]=='-' ) ) 
    {
        life1=life1-1;
        if(life1==0)
        {
            system("cls");
            cout<<"GAME OVER";
            bool gameruning=false;
            return gameruning;
        }    
    }
    else
    {
        return gameruning;
    }    
} 




void gotoxy( short x, short y )
{
    HANDLE hStdout = GetStdHandle(STD_OUTPUT_HANDLE) ;
    COORD position = { x, y } ;
    SetConsoleCursorPosition( hStdout, position ) ;
}




int menue()
{
    int op;
    cout<<" 1-PLAY GAME "<<endl;
    cout<<" 2-HIGGEST SCORE "<<endl;
    cout<<" 3-LAPS "<<endl;
    cout<<" 4-EXIT "<<endl;
    cout<<" PLEASE, ENTER YOUR OPTION "<<endl;
    cin>>op;
    return op;
}




void intro()
{
    cout<<"**********************************************************************************************************"<<endl;
    cout<<"*******************************************";
    cout<<"--  WELCOME  --";
    cout<<"************************************************"<<endl;
}




int higgestscores1(int score1)
{
    int higgestscores;
    if(score1>higgestscores)
    {
        higgestscores=score1;
        return higgestscores;
    }
}








int higgestscores2(int score2)
{
    int higgestscores;
    if(score2>higgestscores)
    {
        higgestscores=score2;
        return higgestscores;
    }
}





bool lapplay()
{
    bool lapplays=false;
    if(map1[0][0]==map1[moveX][moveY])
    {    
        lapplays=true;
        return lapplays;
    }
    else
    {
        return lapplays;
    }      
}




void showlap()
{
    int op;
    cout<<" 1-LAP "<<endl;
    cout<<" 2-LAP "<<endl;
    cout<<" ENTER WHICH LAP YOU WANA SEE"<<endl;
    cin>>op;
    if(op==1)
    {
        showmap1();
    }
    if(op==2)
    {
        showmap2();
    }
}




int option1(bool lapplays)
{   
    int option;
    cout<<" 1-lap"<<endl;
    if(lapplays)
    {
        cout<<" 2-lap"<<endl;
    }
    cout<<" enter ur option "<<endl;
    cin>>option;
    return option;
}




void dollarmovement()
{
    gotoxy(moveX,moveY);
    cout<<" ";
    moveY=moveY-1;
    gotoxy(moveX,moveY);
    cout<<"$";
}




void ghoostmovement1()
{
    gotoxy(ghostx1,ghosty1);
    cout<<" ";
    if(ghostx1<30)
    {
        ghostx1=ghostx1+1;
        Sleep(100);
        gotoxy(ghostx1,ghosty1);
        cout<<"G";
    }
    if(ghostx1>=30)
    {
        ghostx1=ghostx1-1;
        Sleep(100);
        gotoxy(ghostx1,ghosty1);
        cout<<"G";
    }
}





void ghoost1()
{
    gotoxy(ghostx1,ghosty1);
    cout<<"G";
}





void ghoostmovement2()
{
    gotoxy(ghostx2,ghosty2);
    cout<<" ";
    if(ghostDirection2=="Right") 
    {
        ghostx2=ghostx2+1;
    }
    if(ghostDirection2=="Left")
    {
        ghostx2=ghostx2-1;
    }
    if(ghostx2>=rightLimit) 
    {
        ghostDirection2="Left";
    }
    if(ghostx2<=leftLimit)
    {
        ghostDirection2="Right";
    }
    gotoxy(ghostx2,ghosty2);
    cout<<"G";
}






void ghoost2()
{
    gotoxy(ghostx2,ghosty2);
    cout<<"G";
}





int luckyindex1()
{
    int luck=100;
    if(map1[moveX][moveY]=map1[5][23])
    {
        luck=rand()%4;
    }
    return luck;
}




int luckselection1(int luck1,int life1,int score1)   
{
    if(luck1==1)
    {
        life1=life1-1;
        return life1;
    }
    if(luck1==2)
    {
        life1=life1+1;
        return life1;
    }
    if(luck1==3)
    {
        gameruning=false;
    }
    if(luck1==4)
    {
        score1=score1*2;
        return score1;
    }
}






int luckyindex2()
{
    int luck=100;
    if(map1[8][8]==map1[moveX][moveY] || map2[6][6]==map2[moveX][moveY])
    {
        luck=rand()%4;
    }
    return luck;
}




int luckselection2(int luck2,int life2,int score2)   
{
    if(luck2==1)
    {
        life2=life2-1;
        return life2;
    }
    if(luck2==2)
    {
        life2=life2+1;
        return life2;
    }
    if(luck2==3)
    {
        gameruning=false;
    }
    if(luck2==4)
    {
        score2=score2*2;
        return score2;
    }
}




void usermovement()
{
    if(GetAsyncKeyState( VK_LEFT )){
        moveleft();
    }
    if(GetAsyncKeyState( VK_RIGHT )){
        moveright();
    }
    if(GetAsyncKeyState(  VK_UP  )){
        moveup();
    }
    if(GetAsyncKeyState( VK_DOWN )){
        movedown();
    }
}




int scorecounter()
{   
    int scores;
    if(map1[moveX][moveY]=='+' || map2[moveX][moveY]=='+')
    {
        scores=scores+10;
        return scores;
    }
    else if(map1[moveX][moveY]=='.' || map2[moveX][moveY]=='.')
    {
        scores=scores+30;
        return scores;
    }
    else if(map1[moveX][moveY]=='^' || map1[moveX][moveY]=='^')
    {
        scores=scores-5;
        return scores;
    }
}




void printscore(int score)
{
    gotoxy(40,1);
    cout<<"score="<<score;
}




void dollarplacement()
{
    gotoxy(moveX,moveY);
    cout<<"$";
}